This page provides a somewhat broad level summary of the tabs, plots and results you'd see on the output page of a completed BayesWave (BW) run.

## How to get there
To view the interactive output pages from any of the runs you ran on one of the LDG clusters, the first thing you have to do is transfer the results from your completed run to your home directory's `public_html` folder. This means that you have to go to the working directory for the run that you specified to be created from `bayeswave_pipe`, and either copy that directory completely to your `/home/albert.eintsein/public_html`, or just copy the folder(s) that say `trigtime_xxxxxx`. Once that's done, go to your browser and open https://ldas-jobs.ligo.caltech.edu/~albert.einstein/, where you will now find the contents of your public_html folder. Now navigate to your `trigtime_xxxxxx` folder and click on that, and if the BW run completed and exited properly, you should now have the interactive output page open, with the header **BayesWave Output Page** on the top along with the GPS trigger time used. There should also be a list of tabs on the left side which you can click on to access those pages, and a coloured plot on the right showing the log evidence for signal/glitch/noise from the run (this is the default first page/tab, "Model Selection"). Something like [this](https://ldas-jobs.ligo.caltech.edu/~nayyer.raza/LDG-GW150914_osg_v5/trigtime_1126259462.420000076_0.0_0.0_0/)

## Model Selection
This is the main page, showing a brief summary of the results.
- **Detector Names**: showing which detectors have been used in the reconstruction (H1, L1, V1)
- **SNRs of Injections**: if the reconstruction is of an injected signal, then BW also estimates and shows the SNR of the injected signal in each of the detectors, based on the frequency content of the signal and the specified noise properties (e.g. PSD of the simulated noise). (Meg's note: this number is not strictly precise, but gives a good estimate for most purposes.)
- **Log Info**: click on the *see full log file* link and you will find i) details of the BW version and the command line arguments executed, and ii) the run settings that were specified to be used for this job in a nice easy to read list format
- **Signal Evidence**: log Bayes factor values (evidence ratios) indicating which model (signal, glitch, noise) is preferred given the data. For positive signal-glitch or signal-noise values, it means that the signal model is preferred, with the higher the value the stronger the support for the signal model. For negative values the glitch/noise model is preferred.
- **Evidence Plot**: the plot in the centre-right of this page visualizes the aforementioned model preference criteria. It is divided by colour into the three regimes where each of the three models is most preferred. Corresponding to the signal evidence values computed for your run, a black plus sign is plotted in that space of competing signal, glitch and noise models. Note that the glitch-noise boundary runs along the diagonal, as it should (for example if your signal-glitch evidence is -10 and signal-noise evidence is -10, then your glitch-noise evidence is 0 as neither model is preferred over the other and the marker would be right at the boundary).

## Signal Model
This page shows various plots of the reconstructed signal model in each detector.
- **Time Domain Waveforms**: For each detector there is a plot that shows two (or three for injections) whitened waveforms in the time domain (where whitened means normalized with respect to the amplitude spectral density of the detector noise). In green is the injected signal, in grey is the combined signal+glitch+noise data, and in purple is the BW reconstructed signal: the solid line in darkest purple is the median reconstruction, and the lighter shaded bands are the 50% and 90% credible intervals. The time on the x-axis is with respect to the trigger time. The first row of plots will show you a zoomed-in version of the segment of time in which most of the reconstructed signal is found. Clicking on *See full axis: show* will show you the waveforms in the whole time segment analyzed.
- **Power Spectra**: As for the time domain waveforms, in green is the power spectrum of the injected signal, grey is the combined full data, and purple is the recovered signal (median, 50% and 90% CIs). Here for the combined data is also plotted a solid black line which estimates the median PSD, along with dark grey (50%) and light grey (90%) confidence interval bands around the line.
- **Frequency vs. Time**: Median, 50% and 90% CI plotted for the time-frequency track made by the reconstructed signal.
- **Spectrograms**: Q scans of the i) median reconstructed waveform, ii) combined full data, and iii) residuals i.e. combined data - median reconstructed, which should be consistent with noise only if the signal is well recovered. For each of these plots you can select *Other Q resolutions: Show* to see the Q scans for Q=4 and Q=16 as well.

## Glitch Model
This page shows various plots of the reconstructed glitch model in each detector. The plots and their contents follow the same outline as the **Signal Model** page, where now the glitch model waveforms are shown in golden.

## Signal Waveform Moments
On this page are shown the posterior distribution plots of the central waveform moments that BW computes from the reconstructed signal.

At each step in the MCMC BW can produce the reconstructed waveform in terms of time or frequency. To summarize the characteristics of these waveforms at each step we can calculate some of their parameters. Because BW does not fit to any particular astrophysical model, but rather models generic burst signals, it computes some model-independent parameters of the signal that help to summarize the physical content of the waveforms. These are the first and second central moments, defined as:
- central time $`t_c = \int_{-\infty}^{\infty} t \rho (t) dt`$
- central frequency $`f_c = \int_{0}^{\infty} f \rho (f) df`$
- duration $`\Delta t = \left[\int_{-\infty}^{\infty} (t-t_c)^2 \rho (t) dt\right]^{1/2}`$
- bandwidth $`\Delta f = \left[\int_{0}^{\infty} (f-f_c)^2 \rho (f) df\right]^{1/2}`$

where $`\rho(t) = h(t)^2/h_{rss}^2`$ and $`\rho(f) = 2|\tilde{h}(f)^2|/h_{rss}^2`$ are the unit normalized densities, and $`h_{rss}^2 = \int_{-\infty}^{\infty} h(t)^2 dt = \int_{0}^{\infty} 2|\tilde{h}(f)^2| df`$ is the root-sum-squared strain amplitude.

The distributions for these parameters from the recovered waveforms are plotted for each detector (also showing the value computed from the injected signal if doing injection recovery). At the bottom of the page is also plotted the distribution of the recovered SNR of the waveform.

Note that by default BW only samples every 100th step in the chain, and for these plots only the latter half of all samples are used. So for example if the number of iterations specified for the BW job were 4 million, then it will sample a total of 40000 iterations, and then use only the ones from 20000-40000 to produce the distributions.

## Glitch Waveform Moments
On this page are shown the posterior distribution plots of the central waveform moments that BW computes from the reconstructed glitch model. The plots and their contents follow the same outline as the **Signal Waveform Moments** page, where now the glitch model waveform moments are shown in golden.

## Skymap
This page shows the joint posterior distribution of the sky position parameters (right ascension and declination) estimated by BW for the reconstructed signal, projected onto a mollweide skymap. The darker regions show areas of higher probability, and the lighter regions are lower probability. Also shown are the 50% and 90% confidence interval contours. If recovering an injected signal and the sky location of the source is provided to BW, then a star on the map shows this location.

(Note: Don't be alarmed if you don't see a plot here! These skymaps are built on LIGO skymaps, and the compatibility of the two rapidly changes, often leading to the functionality breaking. If skymaps are important for your work, you might have to do some trial and error to see which version of LIGO skymap works and then continue with that.)

## Overlap
If the run involved recovering an injected signal, then BW also outputs this page, showing the histograms of the overlap (also called match), between the waveform of the injected signal and the wavelet reconstruction found by BW.

The overlap is define as
$`O = (h_i|h_r)/\sqrt{(h_i|h_i)(h_r|h_r)}`$, where the notation $`(x|y)`$ denotes a noise-weighted inner product. Thus the overlap characterizes the quality of the waveform reconstruction, measuring the similarity of the (whitened) injected $`h_i`$ and recovered $`h_r`$ waveforms. This value ranges from -1 to 1, with $`O = 1`$ meaning a perfect match, $`O = 0`$ meaning no match, and $`O = -1`$ signifying a perfect anticorrelation.

Similar to the _Waveform Moments_, the histograms for the calculated overlaps are shown for each detector, as well as for the combined network. This is done for both the signal model (purple) and the glitch model (golden).

## Diagnostics
On this page you will find various diagnostic plots that are computed to help provide a peek under the hood of the BW run, should you want to do a quick check or troubleshoot any problems.
- **Likelihood**: The log likelihood value computed for the signal, glitch and noise models, as a function of the (inverse) temperature used in the MCMC chains. BW uses a parallel tempering approach, where multiple chains are run in parallel at different temperatures. The temperature determines the probability of the algorithm for accepting or rejecting the proposed next step in the chain, where the likelihood function is modified such that $`p(s|h) \rightarrow p(s|h)^{1/T}`$. Hot chains explore the entire prior volume, while cooler chains map the peaks of the posterior distribution (the plots on the output page are based on samples drawn from the coldest chain).
- **Model Dimension**: This is the number of wavelets that are placed to reconstruct the signal/glitch. The left two panels show the distribution of the dimension over the downsampled latter half of the chains (similar to the _Waveform Moments_), and the right two panels show how the number of wavelets placed evolves as BW runs. Since only the latter half of the run is sampled, you should see the model dimension should scatter around a constant value, i.e. it should have converged. If the value does not seemed to have reached a steady number and is showing trends in increasing or decreasing across the iterations, it would be a good idea to run the BW reconstruction again with a higher number of iterations specified.
- **Whitened Residuals**: These plots compare the PDF and CDF of the whitened residuals, i.e. when the reconstructed signal or glitch is subtracted from the total signal/glitch+noise, to a standard normal distribution, which is how the white Gaussian noise is expected to be distributed. The distribution should show a close match between the two if BW is capturing the excess power it should due to the presence of the signal/glitch in the data.

## Further Reading
Here are few of the main BW papers for further reading:
- [BayesWave: Bayesian Inference for Gravitational Wave Bursts and Instrument Glitches](https://arxiv.org/abs/1410.3835)
- [BayesLine: Bayesian Inference for Spectral Estimation of Gravitational Wave Detector Noise](https://arxiv.org/abs/1410.3852)
- [Enabling high confidence detections of gravitational-wave bursts](https://arxiv.org/abs/1511.08752)
- [Bayesian reconstruction of gravitational wave bursts using chirplets](https://arxiv.org/abs/1804.03239)
- [The BayesWave analysis pipeline in the era of gravitational wave observations](https://arxiv.org/abs/2011.09494)
